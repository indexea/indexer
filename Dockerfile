FROM --platform=linux/amd64 rust:bookworm as builder
WORKDIR /usr/src
RUN	mkdir ~/.cargo/ && \
	echo '[source.crates-io]' > ~/.cargo/config && \
	echo 'registry = "https://mirrors.ustc.edu.cn/crates.io-index/"' >> ~/.cargo/config && \
	echo 'replace-with = "ustc"' >> ~/.cargo/config && \
	echo '[source.ustc]' >> ~/.cargo/config && \
	echo 'registry = "https://mirrors.ustc.edu.cn/crates.io-index/"' >> ~/.cargo/config
COPY . .
RUN cargo install --path .

FROM --platform=linux/amd64 debian:bookworm-slim
RUN apt-get update && \
	apt-get install -y openssl ca-certificates tzdata && \
	rm -rf /var/lib/apt/lists/*
ENV TZ=Asia/Shanghai
WORKDIR /app
COPY --from=builder /usr/local/cargo/bin/indexer /usr/local/bin/indexer
CMD ["indexer", "-c", "/app/indexer.yml"]
