use std::collections::HashMap;

use log::info;
use sqlparser::ast::SetExpr::Select;
use sqlparser::ast::Statement::Query;
use sqlparser::ast::TableFactor;
use sqlparser::dialect::GenericDialect;
use sqlparser::parser::Parser;

use crate::datasource::record::IndexeaTask;

use super::Task;

pub mod mysql;
pub mod oracle;
pub mod postgres;

const DIALECT: GenericDialect = GenericDialect {};
// or AnsiDialect
pub const TABLE_TASKS: &str = "indexea_tasks";
pub const ERR_TABLE_NOT_EXISTS: &str =
    "indexea_tasks not exists, you need run `indexer -i` to initialize it.";

/// base database operations
trait IDatabase {
    /// init database
    fn init_db(&self, tasks: &HashMap<&String, &Task>) -> Result<(), String> {
        if tasks.len() > 0 {
            let exists = self.tasks_table_exists()?;
            if !exists {
                self.create_tasks_table()?;
                info!(
                    "tasks table created for [\"{}\"]",
                    tasks.iter().next().unwrap().1.datasource
                );
            }
            //check and create trigger
            for task in tasks.iter() {
                self.create_triggers(task.0, task.1)?;
                info!("create triggers for task [\"{}\"] in [\"{}\"]", task.0, task.1.datasource);
            }
        }
        Ok(())
    }

    fn clean_db(&self, tasks: &HashMap<&String, &Task>) -> Result<(), String> {
        for task in tasks.iter() {
            self.drop_triggers(task.0, task.1)?;
        }
        self.drop_tasks_table()?;
        Ok(())
    }

    /// check if table `indexea_tasks` exists
    fn tasks_table_exists(&self) -> Result<bool, String>;

    /// create `indexea_tasks` table
    fn create_tasks_table(&self) -> Result<(), String>;

    /// drop `indexea_tasks` table
    fn drop_tasks_table(&self) -> Result<(), String>;

    /// create triggers for each task
    fn create_triggers(&self, name: &String, task: &Task) -> Result<(), String>;

    /// drop triggers for each task
    fn drop_triggers(&self, name: &String, task: &Task) -> Result<(), String>;

    /// Generate record sql according to use configurations
    fn get_select_sql(&self, config: &Task, task: &IndexeaTask) -> String {
        let value = match task.ftype {
            1 => format!("{}", task.value),
            _ => format!("\"{}\"", task.value),
        };
        if let Some(sql) = config.sql.as_ref() {
            //parse table alias
            let mut t_alias = String::new();
            let ast = Parser::parse_sql(&DIALECT, sql).unwrap();
            let mut has_where = false;
            if let Query(q) = &ast[0] {
                if let Select(s) = &*q.body {
                    //println!("{:?}", s.selection.as_ref().unwrap());
                    has_where = s.selection.is_some(); //check if sql has where clause
                    'table: for twj in &s.from {
                        let t = &twj.relation;
                        if let TableFactor::Table { name, alias, .. } = t {
                            for ident in &name.0 {
                                if ident.value.eq(config.table.as_ref().unwrap()) {
                                    if let Some(a) = alias {
                                        t_alias = a.name.value.clone();
                                        break 'table;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            format!(
                "{} {} {} = {}",
                config.sql.as_ref().unwrap(),
                if has_where { "AND" } else { "WHERE" },
                if t_alias.len() == 0 {
                    task.field.clone()
                } else {
                    format!("{}.{}", t_alias, task.field)
                },
                value
            )
        } else {
            format!(
                "SELECT * FROM {} WHERE {} = {}",
                config.table.as_ref().unwrap(),
                task.field,
                value
            )
        }
    }

    fn list_records_sql(&self, task: &Task, limit: u32, offset: u32, scroll_id: u64) -> String {
        if task.scroll_by_id {
            match task.sql.as_ref() {
                Some(t_sql) => format!(
                    "SELECT * FROM ({}) t WHERE t.{} > {scroll_id} LIMIT {limit}",
                    t_sql,
                    task.primary.as_ref().unwrap()
                ),
                None => format!(
                    "SELECT * FROM {} WHERE {} > {scroll_id} ORDER BY {} LIMIT {limit}",
                    task.table.as_ref().unwrap(),
                    task.primary.as_ref().unwrap(),
                    task.primary.as_ref().unwrap()
                ),
            }
        } else {
            match task.sql.as_ref() {
                Some(t_sql) => format!("{} LIMIT {limit} OFFSET {offset}", t_sql),
                None => format!(
                    "SELECT * FROM {} ORDER BY {} LIMIT {limit} OFFSET {offset}",
                    task.table.as_ref().unwrap(),
                    task.primary.as_ref().unwrap()
                ),
            }
        }
    }
}
