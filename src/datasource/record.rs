use serde_json;
use time::PrimitiveDateTime;

#[derive(PartialEq, Debug)]
pub struct IndexeaTask {
    pub id: i64,
    pub task: String,
    pub field: String,
    pub value: String,
    pub ftype: i8,
    //ops => 1: insert, 2: update, 3: delete
    pub ops: i8,
    pub status: i8,
    pub created: PrimitiveDateTime,
    pub updated: Option<PrimitiveDateTime>,
}

/// use json to store user records
#[derive(PartialEq, Debug)]
pub struct Record {
    /// operation task
    pub task: IndexeaTask,

    /// record value in json
    pub value: serde_json::Value,

    /// index name defined in indexer.yaml [endpoint.indices]
    pub index: String,
}

impl Clone for IndexeaTask {
    fn clone(&self) -> Self {
        let this = IndexeaTask {
            id: self.id,
            task: format!("{}", self.task),
            field: format!("{}", self.field),
            value: format!("{}", self.value),
            ftype: self.ftype,
            ops: self.ops,
            status: self.status,
            created: self.created.clone(),
            updated: self.updated.clone(),
        };
        this
    }
}
