use std::collections::HashMap;
use std::fs::File;
use std::path::Path;

use serde_json::Value;

use crate::datasource::record::Record;
use crate::datasource::{DataSource, IDataSource};
use crate::task::Task;

pub struct FilesDataSource {
    pub path: String,
}

impl IDataSource for FilesDataSource {
    fn new(ds: &DataSource) -> Self
    where
        Self: Sized,
    {
        FilesDataSource { path: ds.path.to_string() }
    }

    /// check and create state directory ('done')
    fn init(&self, tasks: &HashMap<&String, &Task>) -> Result<(), String> {
        let root_path = Path::new(&self.path);
        if let Err(e) = File::create(root_path) {
            return Err(e.to_string());
        }
        let the_path = format!("{}/done", &self.path);
        let done_path = Path::new(&the_path);
        if let Err(e) = File::create(done_path) {
            return Err(e.to_string());
        }
        Ok(())
    }

    /// check the `path` exists
    fn ping(&self) -> Result<bool, String> {
        Ok(Path::new(&self.path).exists())
    }

    fn records(
        &self,
        task: &Task,
        limit: u32,
        offset: u32,
        scroll_id: u64,
    ) -> Result<Vec<Value>, String> {
        Ok(Vec::new())
    }

    fn tasks(&self, name: &String, task: &Task, count: i32) -> Result<Vec<Record>, String> {
        todo!()
    }

    fn finish(&self, records: &Vec<Record>) -> Result<(), String> {
        // move file to [done]
        todo!()
    }

    fn clean(&self, tasks: &HashMap<&String, &Task>) -> Result<(), String> {
        Ok(())
    }
}
