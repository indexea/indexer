use clap::Parser;

const DEFAULT_CONFIG: &str = "indexer.yml";
const DEFAULT_LOG_FILE: &str = "indexer.log";

#[derive(Parser, Debug)]
#[command(version = env ! ("CARGO_PKG_VERSION"), about = env ! ("CARGO_PKG_DESCRIPTION"), long_about = None)]
pub struct CmdLineArgs {
    #[arg(short, long, value_name = "CONFIG FILE", default_value_t = DEFAULT_CONFIG.to_string())]
    config: String,

    #[arg(short, long, value_name = "LOG FILE", default_value_t = DEFAULT_LOG_FILE.to_string())]
    log: String,

    #[arg(short = 't', long, default_value_t = false, help = "to test configurations.")]
    test: bool,

    #[arg(short = 'v', long, default_value_t = false, help = "to enable verbose mode.")]
    verbose: bool,

    #[arg(
        short = 'i',
        long,
        default_value_t = false,
        help = "initialize tasks table and triggers."
    )]
    init: bool,

    #[arg(
        short = 'd',
        long,
        default_value_t = false,
        help = "start indexer in daemon for increment index."
    )]
    start: bool,

    #[arg(short = None, long, default_value_t = false, help = "clean indexer tables and triggers")]
    clean: bool,

    #[arg(short = None, long, default_value_t = false, help = "stop indexer in daemon.")]
    stop: bool,
}

impl CmdLineArgs {
    pub fn start_args(&self) -> Vec<String> {
        vec![String::from("-c"), self.config.clone(), String::from("-l"), self.log.clone()]
    }

    pub fn config(&self) -> &str {
        self.config.as_str()
    }

    pub fn log(&self) -> &str {
        self.log.as_str()
    }

    pub fn is_init(&self) -> bool {
        self.init
    }

    pub fn is_test(&self) -> bool {
        self.test
    }

    pub fn is_verbose(&self) -> bool {
        self.verbose
    }

    pub fn is_start(&self) -> bool {
        self.start
    }

    pub fn is_stop(&self) -> bool {
        self.stop
    }

    pub fn is_clean(&self) -> bool {
        self.clean
    }
}

impl Clone for CmdLineArgs {
    fn clone(&self) -> CmdLineArgs {
        let data = CmdLineArgs {
            config: format!("{}", self.config),
            log: format!("{}", self.log),
            test: self.test,
            verbose: self.verbose,
            init: self.init,
            start: self.start,
            clean: self.clean,
            stop: self.stop,
        };
        data
    }
}
