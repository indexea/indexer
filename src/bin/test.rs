use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader};

use indexer::summary::summarize;

fn main() -> io::Result<()> {
    // 打开文件
    let file = File::open("/Users/query_result.csv")?;
    let reader = BufReader::new(file);

    // 逐行打印
    for line in reader.lines() {
        let s_line = line.expect("");
        println!("{}", s_line);
        let res = summarize(s_line.as_str(), &[], 2);
        println!("{}", res);
        println!("=================================");
    }

    Ok(())
}
