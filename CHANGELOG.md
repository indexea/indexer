# CHANGELOG

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Released 1.2.0]

### Added

- Add preprocessor for Markdown and extract images to new field
- Add scroll_by_id option for large table
- Add connection pool for MySQL
- Add connection pool for PostgreSQL
- Add execute time in log
- auto clean records of indexer_tasks table
- Add extract_summary_to_field option for preprocessor (html_strip and markdown)
- Update tasks status to failed when object not found

### Bugfix

- fix read sql failed when run in incremental mode

## [Release 1.1.0]

### Added

- Add preprocessor for HTML strip, and support extract images to new field
- Add preprocessor for string field to split into array

### Bugfix

- Fix can only push 1000 records on initial sync.
- Fix indexer show error detail when call remote api.