## indexer

`indexer` 是一个实现将客户数据（支持包括 SQL 等多种数据库、文件目录、队列）自动同步到 `indexea` 的工具。

`indexer` 采用 rust 语言开发，体积小、支持跨平台，简单轻便，资源占用少，同时对业务系统的侵入很小。

`indexer` 提供非常灵活的配置 (`indexer.yml`)，以非常高效方式将业务数据同步到 `indexea`。

该工具基于 MIT 许可证开源，开源地址：[https://gitee.com/indexea/indexer](https://gitee.com/indexea/indexer)

## 使用说明

### 编译

安装 rust 环境

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

配置 rust 镜像库(可选)

$HOME/.cargo/config

```toml
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"
replace-with = 'tuna'
[source.tuna]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"
```

下载源码并编译

```shell
git clone https://gitee.com/indexea/indexer.git
cd indexer
cargo build --release
```

### 配置文件

`indexer` 使用 YAML 文件(`indexer.yml`)进行灵活的自定义配置，配置包括三部分内容：

| 序号 | 配置名        | 配置说明         | 其他说明                                           |
|----|------------|--------------|------------------------------------------------|
| 1  | datasource | 数据源定义        | 允许配置多个数据库，当前支持 MySQL 和 Postgres                |
| 2  | endpoint   | Indexea 服务配置 | 只能配置单个服务,如果需要使用多个服务，可以运行多个 indexer 的实例         |
| 3  | tasks      | 同步任务配置       | 配置 datasource 的数据如何同步到 endpoint 中，可以配置一到多个同步任务 |

**indexer.yml**

```yaml
datasource:
  mydb: # datasource name
    source: "mysql" # mysql,postgres,oracle
    url: "mysql://account:passwd@localhost:3306/mydb"

endpoint:
  url: "https://api.indexea.com/v1/" # Indexea Gateway API 
  oauth_url: "https://api.indexea.com/oauth"  # Indexea Gateway OAuth
  client_id: "smxuk5rvrfjkzhrf1244"  # client id (详见应用设置的第三方应用管理)
  client_secret: "gqee9yh2igplhkb9deq9ngqmufu19ivaq4mp2492"  # client secret
  indices:
    repositories: # index name
      app: "tw6mi8il" # app ient
      index: 1005     # index id
    blogs:
      app: "tw6mi8il"
      index: 1006

tasks:
  repositories: # task name
    datasource: "mydb" # datasource  name, refer to $datasource.mydb
    index: "repositories" # index name, refer to $endpoint.indices.repositories
    table: "repositories" # table name
    primary: "id" # primary field name of table 
    fields:
      tags:
        split: # split field value to multi values
          new_field_name: "tags" # 新字段名(可选)
          delimiter: "," # 字段值分隔符
          type: string # 字段值类型, string or (integer, long, number) or (float, double)
      intro:
        html_strip: # 去除 html 标签
          new_field_name: "detail" # 新字段名(可选)
          extract_images_to_field: "images" # 提取图片到新字段(可选)
      detail:
        markdown: # markdown 转为纯文本
          new_field_name: "detail" # 新字段名
          extract_images_to_field: "images" # 提取图片到新字段(可选)
          extract_summary_to_field: "summary" # 提取摘要到新字段(可选,速度非常慢，生成一次的时间在几百毫秒)
          summary_sentences: 2 # 摘要句子数(可选)
    interval: 1000 # check interval in mills-second
  blogs:
    datasource: "mydb"
    index: "blogs"
    table: "blogs"
    primary: "id"
    sql: "SELECT b.*, a.name AS author_name FROM blogs b LEFT JOIN authors a ON b.author = a.id" ##指定 SQL ，否则默认为 SELECT * FROM <table>
    scroll_by_id: true # 使用主键滚动模式(如果表记录非常大，启用该配置性能最佳，要求主键字段必须是增长的数值字段)
    fields:
      author_name: "user" # 字段名映射，例：在 indexea 将使用 user 字段名来表示 author_name 的值
      password: "" # 删除某个字段，一些敏感信息不需要进行搜索的可以通过这种方式避免同步到 indexea
      status: # 自定义字段值映射
        0: "pending"
        1: "open"
        2: "block"
        3: "delete"
    interval: 2000 
```

### 程序参数

**-c <indexer.yml>**  
指定配置文件路径

**-l <indexer.log>**  
指定日志文件路径

**-t --test**  
测试配置是否正确

**-i --init**   
在本地数据源上建立 indexer 必备的信息，例如在数据库上建 `indexea_tasks` 表和触发器等。
然后执行数据首次同步，也就是全量的数据同步，如果使用该参数，需要用户进行二次确认

**-d --start**  
进入守护进程方式开始增量数据同步

**--clean**  
清除 indexer 生成的表、函数和触发器，  
当不再使用 indexer 时可以使用该参数来清除数据。

**--stop**  
停止守护进程

### 运行流程

#### 初始化和全量同步

命令：**indexer -i**  
首次使用前需要进行配置，你可以运行 `indexer -t` 来测试配置文件是否正确，然后运行 `indexer -i` 来执行初始化。

初始化主要包括三部分：

1. 在你指定的数据源中建 `indexea_tasks` 任务表，表结构如下表所示
2. 创建每个任务对应的数据增删改的触发器，触发器的主要任务是将变更数据写入 `indexea_tasks` 表中
3. 执行全量的数据同步（同步完成后 indexer 进程自动终止，同步的时间取决于数据量大小）

**[indexea_tasks] 表结构 (MySQL)**

| 字段名    | 类型          | 说明              | 其他说明                        |
|--------|-------------|-----------------|-----------------------------|
| id     | bigint      | 自增长主键           |                             |
| task   | varchar(32) | 对应 yml 配置中的任务名称 |                             |
| field  | varchar(32) | 对象主键字段名         |                             |
| value  | varchar(64) | 字段值             |                             |
| type   | tinyint     | 字段类型            | 1 数值，2 字符串                  |
| ops    | tinyint     | 操作类型            | 1 添加，2 修改, 3 删除             |
| status | tinyint     | 处理状态            | 0 未处理，1 已处理, 2 处理失败, 3 参数错误 |

其他数据库的表结构与 MySQL 同。

**数据库触发器(MySQL)**

```sql
CREATE TRIGGER indexea_after_insert_<task>
AFTER
DELETE
ON <table> FOR EACH ROW
INSERT INTO indexea_tasks(`task`,`field`,`value`,`type`,`status`) VALUES('<task>', '<field>', NEW.<field>, 1, 0);

CREATE TRIGGER indexea_after_update_<task>
AFTER
UPDATE
    ON < table > FOR EACH ROW
INSERT
INTO indexea_tasks(`task`, `field`, `value`, `type`, `status`)
VALUES ('<task>', '<field>', OLD.<field>, 2, 0);

CREATE TRIGGER indexea_after_delete_<task>
AFTER
DELETE
ON <table> FOR EACH ROW
INSERT INTO indexea_tasks(`task`,`field`,`value`,`type`,`status`) VALUES('<task>', '<field>', OLD.<field>, 3, 0);
```

其中：

- \<task> 对应 indexer.yml 中的任务名称
- \<table> 对应 indexer.yml 中任务对应的表名
- \<field> 对应 indexer.yml 中表配置的主键名称 (primary)

其他数据库原理与 MySQL 相同。

**如果你不希望通过触发器来自动同步变更数据，可以删除触发器，并自行通过程序将变更数据写入 indexea_tasks 表即可。**

#### 自动增量同步

一旦完成了初始化配置和全量同步后，你可以通过 `indexer -d` 命令启动服务进入自动增量同步的守护进程。
守护进程会定时读取 `indexea_tasks` 表中记录即时同步到 `indexea`。

你可以通过查看 `indexea.log` 来查看服务运行状态。

你可以使用 `indexer --stop` 或者 `kill` 命令来停止服务运行。

### 在 Docker 中运行

#### 构建

```bash
docker build -t indexer:latest .
```

#### 运行

```bash
docker run --name indexer --volume $(pwd)/indexer.yml:/app/indexer.yml indexer:latest
```
